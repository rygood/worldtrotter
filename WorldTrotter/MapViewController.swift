//
//  MapViewController.swift
//  WorldTrotter
//
//  Created by Ryan Goodlett on 1/27/17.
//  Copyright © 2017 Bits n' Pixels LLC. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    var mapView: MKMapView!
    var locationManager: CLLocationManager!

    // MARK: - Segmented Control
    func mapTypeChanged(_ segControl: UISegmentedControl) {
        switch segControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .hybrid
        case 2:
            mapView.mapType = .satellite
        default:
            break
        }
    }

    // MARK: - Current Location Button
    func updateCurrentUserLocation(_ sender: UIButton) {
        locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
    }

    // MARK: - View Lifecycle

    override func loadView() {
        // Create a map view
        mapView = MKMapView()
        // Create the location manager
        locationManager = CLLocationManager()
        // Set the delegate to this controller
        mapView.delegate = self

        // Set it as *the* view of this view controller
        view = mapView

        // MARK: Segmented Control
		let standardString = NSLocalizedString("Standard", comment: "Standard map view")
		let satelliteString = NSLocalizedString("Satellite", comment: "Satellite map view")
		let hybridString = NSLocalizedString("Hybrid", comment: "Hybrid map view")
        let segmentedControl = UISegmentedControl(items: [standardString, satelliteString, hybridString])
        segmentedControl.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        segmentedControl.selectedSegmentIndex = 0

        // add target-action
        segmentedControl.addTarget(self, action: #selector(MapViewController.mapTypeChanged(_:)), for: .valueChanged)

        segmentedControl.translatesAutoresizingMaskIntoConstraints =  false
        view.addSubview(segmentedControl)

        // Constraints
        let topConstraint = segmentedControl.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 8)
        let margins = view.layoutMarginsGuide
        let leadingConstraint = segmentedControl.leadingAnchor.constraint(equalTo: margins.leadingAnchor)
        let trailingConstraint = segmentedControl.trailingAnchor.constraint(equalTo: margins.trailingAnchor)

        topConstraint.isActive = true
        leadingConstraint.isActive = true
        trailingConstraint.isActive = true
        // END: Segmented Control

        // MARK: Location Button
        let locationButton = UIButton(type: .system)
        locationButton.setImage(UIImage(named: "LocationIcon"), for: .normal)
        locationButton.translatesAutoresizingMaskIntoConstraints = false

        // add target-action
        locationButton.addTarget(self, action: #selector(MapViewController.updateCurrentUserLocation(_:)), for: .touchUpInside)
        view.addSubview(locationButton)

        // Constraints
        let bottomConstraint = locationButton.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -56)
        let trailingLocationConstraint = locationButton.trailingAnchor.constraint(equalTo: margins.trailingAnchor)

        bottomConstraint.isActive = true
        trailingLocationConstraint.isActive = true

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        print("MapViewController loaded its view.")
    }

}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let zoomedInCurrentLocation = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 500, 500)
        mapView.setRegion(zoomedInCurrentLocation, animated: true)
    }
}
