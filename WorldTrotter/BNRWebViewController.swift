//
//  BNRWebViewController.swift
//  WorldTrotter
//
//  Created by Ryan Goodlett on 1/27/17.
//  Copyright © 2017 Bits n' Pixels LLC. All rights reserved.
//

import UIKit
import WebKit

class BNRWebViewController: UIViewController {
    var webView: WKWebView!

    // MARK: - View Lifecycle
    override func loadView() {
        // Create a web view
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        view = webView
    }
	
	override func viewDidLoad() {
		super.viewDidLoad()
		let myURL = URL(string: "https://www.bignerdranch.com")
		let myRequest = URLRequest(url: myURL!)
		webView.load(myRequest)
	}
}
