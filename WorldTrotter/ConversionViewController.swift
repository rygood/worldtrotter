//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by Ryan Goodlett on 1/25/17.
//  Copyright © 2017 Bits n' Pixels LLC. All rights reserved.
//

import UIKit

class ConversionViewController: UIViewController {
	@IBOutlet var celsiusLabel: UILabel!
	@IBOutlet var textField: UITextField!
	var fahrenheitValue: Measurement<UnitTemperature>? {
		didSet {
			updateCelsiusLabel()
		}
	}
	
	var celsiusValue: Measurement<UnitTemperature>? {
		if let fahrenheitValue = fahrenheitValue {
			return fahrenheitValue.converted(to: .celsius)
		} else {
			return nil
		}
	}
	
	let numberFormatter: NumberFormatter = {
		let nf = NumberFormatter()
		nf.numberStyle = .decimal
		nf.minimumFractionDigits = 0
		nf.maximumFractionDigits = 1
		return nf
	}()
	
	let darkBackgroundColor = UIColor(red:0.003, green:0.273, blue:0.328, alpha:1)
	let lightBackgroundColor = UIColor(red:0.992, green:0.964, blue:0.89, alpha:1)
	
	@IBAction func fahrenheitFieldEditingChanged(_ textField: UITextField) {
		if let text = textField.text, let number = numberFormatter.number(from: text)	{
			fahrenheitValue = Measurement(value: number.doubleValue, unit: .fahrenheit)
		} else {
			fahrenheitValue = nil
		}
	}
	
	func updateCelsiusLabel() {
		if let celsiusValue = celsiusValue {
			celsiusLabel.text = numberFormatter.string(from: NSNumber(value: celsiusValue.value))
		} else {
			celsiusLabel.text = "???"
		}
	}
	
	@IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
		textField.resignFirstResponder()
	}
	
	// MARK: View Lifecycle
	override func viewDidLoad() {
		super.viewDidLoad()
		
		print("ConversionViewController loaded its view.")
		
		updateCelsiusLabel()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		
		if view.backgroundColor != lightBackgroundColor {
			view.backgroundColor = lightBackgroundColor
		} else {
			view.backgroundColor = darkBackgroundColor
		}
	}
}

extension ConversionViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let currentLocale = Locale.current
		let decimalSeparator = currentLocale.decimalSeparator ?? "."
		
		let existingTextHasDecimalSeparator = textField.text?.range(of: decimalSeparator)
		let replacementTextHasDecimalSeparator = string.range(of: decimalSeparator)
		let abcCharSet = CharacterSet(charactersIn: UnicodeScalar("A")...UnicodeScalar("z"))
		let replacementTextHasABCCharacters = string.rangeOfCharacter(from: abcCharSet)
		
		
		if (existingTextHasDecimalSeparator != nil && replacementTextHasDecimalSeparator != nil)
			|| replacementTextHasABCCharacters != nil {
			return false
		} else {
			return true
		}
	}
}
